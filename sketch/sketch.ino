#include <VarSpeedServo.h>

const unsigned int servoPin = 3;
const unsigned int servoSpeed = 55;

VarSpeedServo myservo;

void setup() {
  myservo.attach(servoPin);
}

void loop() {
  myservo.write(160, servoSpeed, true);
  myservo.write(40, servoSpeed, true);
}







